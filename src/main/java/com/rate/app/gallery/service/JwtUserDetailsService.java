package com.rate.app.gallery.service;

import com.rate.app.gallery.model.User;
import com.rate.app.gallery.model.UserDTO;
import com.rate.app.gallery.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;



    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByName(username);
        if(user != null)
            return new org.springframework.security.core.userdetails.User(user.getName(),user.getPassword(),new ArrayList<>());
        else
            throw new UsernameNotFoundException("User not found with username " + username);

    }

    public List<Object> save(UserDTO userDTO){
        User foundUser = userRepository.findByName(userDTO.getName());
        String message;
        List<Object> responseList =  new ArrayList<>();
        Object responseObject;
        if(foundUser != null){
            message = "USERNAME_ALREADY_USED";
            responseObject = foundUser;
            responseList.add(message);
            responseList.add(responseObject);
            return responseList;
        }
        User encryptedUser = new User(userDTO.getName(), passwordEncoder.encode(userDTO.getPassword()));
        foundUser = userRepository.save(encryptedUser);
        message = "NEW_USER_SAVED";
        responseObject = foundUser;
        responseList.add(message);
        responseList.add(responseObject);
        return responseList;
    }
}
