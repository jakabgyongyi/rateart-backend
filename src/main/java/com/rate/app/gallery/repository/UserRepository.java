package com.rate.app.gallery.repository;

import com.rate.app.gallery.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {
    public User findByName(String name);
    public User findByPassword(String password);
}
