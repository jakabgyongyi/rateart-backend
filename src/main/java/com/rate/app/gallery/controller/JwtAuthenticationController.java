package com.rate.app.gallery.controller;

import com.rate.app.gallery.config.JwtTokenUtil;
import com.rate.app.gallery.model.JwtRequest;
import com.rate.app.gallery.model.JwtResponse;
import com.rate.app.gallery.model.UserDTO;
import com.rate.app.gallery.service.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
public class JwtAuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;


    @PostMapping("/authenticate")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest jwtRequest) {
        ResponseEntity.BodyBuilder builder;
        String message;

        try{
            authenticate(jwtRequest.getName(),jwtRequest.getPassword());
        }catch (Exception e){
            builder = ResponseEntity.status(HttpStatus.UNAUTHORIZED);
            message = "DISABLED_USER/BAD_CREDENTIALS";
            return builder.body(message);
        }

        try {

            //chestie ciudata: in punctul asta deja nu ai cum sa ajungi cu bad username
            UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(jwtRequest.getName());
            String token = jwtTokenUtil.generateToken(userDetails);
            return ResponseEntity.ok(new JwtResponse(token));

        } catch (UsernameNotFoundException e) {
            builder = ResponseEntity.status(HttpStatus.UNAUTHORIZED);
            message = "USERNAME_NOT_FOUND";
        }
        return builder.body(message);
    }

    @PostMapping("/register")
    public ResponseEntity<?> saveUser(@RequestBody UserDTO userDTO) {
        List<Object> response = jwtUserDetailsService.save(userDTO);
        ResponseEntity.BodyBuilder builder;
        if (response.get(0).equals("NEW_USER_SAVED")) {
            return ResponseEntity.ok(response.get(1));
        } else {
            builder = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return builder.body(response.get(1));
    }

    private void authenticate(String username, String password)throws Exception{
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new DisabledException("DISABLED_USER",e);

        } catch (BadCredentialsException e) {
            throw new BadCredentialsException("BAD_CREDENTIALS",e);
        }
    }
}
