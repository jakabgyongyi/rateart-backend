package com.rate.app.gallery.model;

public class JwtResponse {
    private String jwtToken;

    public JwtResponse(String jwtToken) {
        this.jwtToken = jwtToken;
    }

    public JwtResponse() {
    }

    public String getJwtToken() {
        return jwtToken;
    }
}
